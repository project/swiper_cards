(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.swiper_cards = {
        attach: function (context) {
            const $context = $(context);

            // Helper function to initialize Swiper instances
            function initializeSwiper(selector, config) {
                $(once('swiper_cards', $context.find(selector), context)).each(function () {
                    const $swiperElement = $(this);
                    if ($swiperElement.length > 0) {
                        new Swiper($swiperElement, config);
                    }
                });
            }

            // Layout 1: Coverflow effect
            initializeSwiper('.swiper-container-layout-1', {
                effect: "coverflow",
                grabCursor: 'TRUE',
                loop: 'TRUE',
                centeredSlides: 'TRUE',
                slidesPerView: "auto",
                coverflowEffect: {
                    rotate: 20,
                    stretch: 0,
                    depth: 350,
                    modifier: 1,
                    slideShadows: 'TRUE'
                },
                pagination: {
                    el: ".swiper-pagination"
                }
            });

            // Layout 2: Cards effect
            initializeSwiper('.swipercardsblock-container-layout-2', {
                effect: "cards",
                grabCursor: 'TRUE',
                loop: 'TRUE'
            });

            // Layout 3: Responsive slidesPerView
            initializeSwiper('.swipercardsblock-container-layout-3 .swiper-container', {
                slidesPerView: getSlidesPerView(),
                spaceBetween: 0,
                loop: 'TRUE',
                loopedSlides: 16,
                speed: 900,
                autoplay: {
                    delay: 5000,
                    disableOnInteraction: 'TRUE'
                },
                centeredSlides: 'TRUE'
            });

            // Adjust slidesPerView on window resize for Layout 3
            $(window).resize(function () {
                const swiperInstances = Swiper.instances || [];
                swiperInstances.forEach(swiper => {
                    if (swiper.el.classList.contains('swipercardsblock-container-layout-3')) {
                        swiper.params.slidesPerView = getSlidesPerView();
                        swiper.update();
                    }
                });
            });

            // Layout 4: Coverflow effect
            initializeSwiper('.swiper-container-l4', {
                effect: "coverflow",
                grabCursor: 'TRUE',
                loop: 'TRUE',
                centeredSlides: 'TRUE',
                slidesPerView: "auto",
                speed: 900,
                coverflowEffect: {
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: 'TRUE'
                },
                pagination: {
                    el: ".swiper-pagination"
                }
            });

            // Helper function to calculate slidesPerView based on window width
            function getSlidesPerView() {
                const width = $(window).width();
                if (width >= 1700) { return 7;
                }
                if (width >= 1560) { return 6;
                }
                if (width >= 1400) { return 5;
                }
                if (width >= 1060) { return 4;
                }
                if (width >= 800) { return 3;
                }
                if (width >= 560) { return 2;
                }
                return 1;
            }
        }
    };
})(jQuery, Drupal, drupalSettings);
