<?php

namespace Drupal\swiper_cards\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Swiper Cards.
 *
 * @Block(
 *   id="swiper_cards",
 *   admin_label = @Translation("Swiper Cards"),
 * )
 */
class SwiperCardsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The file storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructor for the custom plugin.
   *
   * Initializes the plugin with the given configuration, plugin ID,
   * and plugin definition.
   * Also injects the file storage and file URL generator services
   * for use within the plugin.
   *
   * @param array $configuration
   *   The configuration array for the plugin.
   * @param string $plugin_id
   *   The unique ID for the plugin.
   * @param array $plugin_definition
   *   The plugin definition containing metadata.
   * @param \Drupal\Core\Entity\EntityStorageInterface $file_storage
   *   The service used for file storage operations.
   * @param \Drupal\Core\Url\FileUrlGeneratorInterface $file_url_generator
   *   The service used to generate file URLs.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $file_storage, FileUrlGeneratorInterface $file_url_generator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileStorage = $file_storage;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'swiper_cards' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $items = [];
    if (isset($config['swiper_cards_data'])) {
      $items = $config['swiper_cards_data'];
      if ($items != '') {
        $items = json_decode($items);
      }
    }

    $form['swiper_card_header'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Swiper cards header content'),
      '#description' => $this->t('Swiper cards header content'),
      '#default_value' => $config['swiper_card_header'] ?? '',
      '#format' => $config['swiper_card_header_format'] ?? 'basic_html',
    ];

    $form['swiper_card_layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Swiper Cards Display Layout'),
      '#options' => [
        'layout_1' => $this->t('Layout 1'),
        'layout_2' => $this->t('Layout 2'),
        'layout_3' => $this->t('Layout 3'),
        'layout_4' => $this->t('Layout 4'),
      ],
      '#description' => $this->t('Swiper Card Layout'),
      '#default_value' => $config['swiper_card_layout'] ?? 'layout_1',
    ];

    $form['has_background'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Has Background Color'),
      '#description' => $this->t('Swiper Cards Container has Background Color'),
      '#default_value' => $config['has_background'] ?? 'false',
    ];

    $form['background_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Swiper Cards Background Color'),
      '#description' => $this->t('Swiper Cards Container Background Color'),
      '#default_value' => $config['background_color'] ?? '#212121',
      '#states' => [
        'visible' => [
          ':input[name="settings[has_background]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['#tree'] = TRUE;

    $form['items_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Swiper Cards'),
      '#prefix' => '<div id="items-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    if (!$form_state->has('num_items')) {
      if (is_array($items)) {
        $count_items = count($items);
      }
      else {
        $count_items = 0;
      }
      $form_state->set('num_items', $count_items);
    }
    $number_of_items = $form_state->get('num_items');

    for ($i = 0; $i < $number_of_items; $i++) {

      $is_active_row = $form_state->get("row_" . $i);

      if ($is_active_row != 'inactive') {

        $j = $i + 1;
        $form['items_fieldset']['items'][$i] = [
          '#type' => 'details',
          '#title' => $this->t('Swiper Card @j', ['@j' => $j]),
          '#prefix' => '<div id="items-fieldset-wrapper">',
          '#suffix' => '</div>',
          '#open' => TRUE,
        ];

        $form['items_fieldset']['items'][$i]['card_image'] = [
          '#title' => $this->t('Card image'),
          '#type' => 'managed_file',
          '#upload_location' => 'public://module-images/home-slider-images/',
          '#multiple' => FALSE,
          '#description' => $this->t('Allowed extensions: png jpg jpeg.'),
          '#upload_validators' => [
            'file_validate_is_image' => [],
            'file_validate_extensions' => ['png jpg jpeg'],
            'file_validate_size' => [25600000],
          ],
          '#theme' => 'image_widget',
          '#preview_image_style' => 'medium',
          '#default_value' => (isset($items[$i]->card_image[0])) ? [$items[$i]->card_image[0]] : NULL,
        ];

        $form['items_fieldset']['items'][$i]['card_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Card title'),
          '#description' => $this->t('Card title'),
          '#default_value' => $items[$i]->card_title ?? '',
        ];

        $form['items_fieldset']['items'][$i]['card_subtitle'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Card subtitle'),
          '#description' => $this->t('Card subtitle'),
          '#default_value' => $items[$i]->card_subtitle ?? '',
        ];

        $form['items_fieldset']['items'][$i]['card_description'] = [
          '#type' => 'text_format',
          '#title' => $this->t('Card description'),
          '#description' => $this->t('Card description'),
          '#default_value' => (isset($items[$i]->card_description->value)) ? $items[$i]->card_description->value : '',
          '#format' => (isset($items[$i]->card_description)) ? $items[$i]->card_description->format : 'basic_html',
        ];

        $form['items_fieldset']['items'][$i]['weight'] = [
          '#type' => 'number',
          '#min' => 1,
          '#max' => 120,
          '#title' => $this->t('Swiper Card order weight'),
          '#description' => $this->t('The weight field can be used to provide customized sorting of swiper_cards.'),
          '#default_value' => $items[$i]->weight ?? $j,
        ];

        $form['items_fieldset']['items'][$i]['remove_single_item'] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove item'),
          '#name' => 'row_' . $i,
          '#submit' => [[$this, 'removeItemCallback']],
          '#ajax' => [
            'callback' => [$this, 'addmoreCallback'],
            'wrapper' => 'items-fieldset-wrapper',
          ],
          '#button_type' => 'danger',
        ];
      }
    }

    $form['items_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['items_fieldset']['actions']['add_item'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add swiper card'),
      '#submit' => [[$this, 'addOne']],
      '#ajax' => [
    // '\Drupal\swiper_cards\Plugin\Block\ALaUneBlock::addmoreCallback',
        'callback' => [$this, 'addmoreCallback'],
        'wrapper' => 'items-fieldset-wrapper',
      ],
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeItemCallback(array &$form, FormStateInterface $form_state) {
    $button_clicked = $form_state->getTriggeringElement()['#name'];

    $form_state->set($button_clicked, 'inactive');

    $form_state->setRebuild();

  }

  /**
   * Increments the number of items in the form state.
   *
   * This function retrieves the current number of items from the
   * form state, increments it by one, updates the form state with
   * the new number, and triggers a rebuild of the form to reflect the change.
   *
   * @param array $form
   *   The form array, passed by reference, which may be modified
   *   by the function.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state, used to store and retrieve form values.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    $add_button = $number_of_items + 1;
    $form_state->set('num_items', $add_button);
    $form_state->setRebuild();
  }

  /**
   * AJAX callback to rebuild a specific part of the form.
   *
   * This function handles an AJAX request to update and return
   * the `items_fieldset` section of the form. The `$form` parameter
   * passed here represents the entire form, as opposed to a subform
   * that is passed in non-AJAX callbacks.
   *
   * @param array $form
   *   The entire form array, passed by reference, which may be
   *   modified by the callback.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state, used to store and retrieve form values.
   *
   * @return mixed
   *   The specific part of the form to be re-rendered, in
   *   this case, `items_fieldset` within `settings`.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    // The form passed here is the entire form, not the subform that is
    // passed to non-AJAX callback.
    return $form['settings']['items_fieldset'];
  }

  /**
   * Decrements the number of items in the form state and rebuilds the form.
   *
   * This function checks the current number of items in the form
   * state, decrements it by one (if it is greater than 1), updates
   * the form state with the new value, and triggers a rebuild of
   * the form to reflect the change.
   *
   * @param array $form
   *   The form array, passed by reference, which may be modified
   *   by the function.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state, used to store and retrieve form values.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    if ($number_of_items > 1) {
      $remove_button = $number_of_items - 1;
      $form_state->set('num_items', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * Decrements the number of items in the form state by one & form rebuild.
   *
   * This function checks the current number of items in the form state,
   * and if the count is greater than 1, it decreases the count by one.
   * The updated count is then saved in the form state, and the form is
   * marked for a rebuild to reflect the changes.
   *
   * @param array $form
   *   The form array, passed by reference, which may be modified
   *   by the callback.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state, used to store and retrieve form values.
   */
  public function removeCallbackSingle(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    if ($number_of_items > 1) {
      $remove_button = $number_of_items - 1;
      $form_state->set('num_items', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $swiper_cards_data_config = $this->configuration['swiper_cards_data'] ?? '';
    $swiper_cards_old_image_ids = swiper_cards_old_image_ids($swiper_cards_data_config);

    $current_image_ids = [];
    $items = [];

    $values = $form_state->getValues();
    foreach ($values as $key => $value) {

      if ($key === 'items_fieldset') {
        if (isset($value['items']) && !empty($value['items'])) {
          $items = $value['items'];

          usort($items, function ($x, $y) {
            if (is_numeric($x['weight']) && is_numeric($y['weight'])) {
              return $x['weight'] - $y['weight'];
            }
          });

          foreach ($items as $item_key => $item) {
            if (trim($item['card_title']) === '') {
              unset($items[$item_key]);
            }
            else {
              if (!is_numeric($item['weight'])) {
                $items[$item_key]['weight'] = 1;
              }

              if (isset($item['card_image'][0])) {
                $image_id = $item['card_image'][0];
                $file = $this->fileStorage->load($image_id);
                $file->setPermanent();
                $file->save();
                $current_image_ids[] = $image_id;
              }
            }
          }

          $swiper_cards_data = array_values($items);
          $swiper_cards_data = json_encode($swiper_cards_data);

          $this->configuration['swiper_cards_data'] = $swiper_cards_data;
        }
        else {
          $this->configuration['swiper_cards_data'] = '';
        }

        // Remove old images.
        $result = array_diff($swiper_cards_old_image_ids, $current_image_ids);
        if (!empty($result)) {
          foreach ($result as $card_image_id) {
            $file = $this->fileStorage->load($card_image_id);
            $file->setTemporary();
            $file->save();
          }
        }
      }
    }

    $this->configuration['swiper_card_header'] = $values['swiper_card_header']['value'] ?? '';
    $this->configuration['swiper_card_header_format'] = $values['swiper_card_header']['format'];
    $this->configuration['swiper_card_layout'] = $values['swiper_card_layout'];
    $this->configuration['has_background'] = $values['has_background'];
    $this->configuration['background_color'] = $values['background_color'];

  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $configuration = $this->configuration;
    $swiper_cards_data_config = $configuration['swiper_cards_data'] ?? '';

    $swiper_card_layout = $configuration['swiper_card_layout'] ?? 'layout_1';

    $default_data = [
      'swiper_card_header' => $configuration['swiper_card_header'] ?? '',
    ];
    $background_color = '';
    if ($configuration['has_background']) {
      $background_color = $configuration['background_color'];
    }
    $default_data['background_color'] = $background_color;

    $swiper_cards_data = [];
    if (!empty($swiper_cards_data_config)) {
      $swiper_cards_data = json_decode($swiper_cards_data_config, TRUE);

      foreach ($swiper_cards_data as &$swiper_card) {
        $card_image_id = $swiper_card['card_image'][0] ?? '';
        $swiper_card['image_url'] = '';

        if (!empty($card_image_id)) {
          $file = $this->fileStorage->load($card_image_id);
          if ($file) {
            $swiper_card['image_url'] = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
          }
        }
      }
    }

    $build = [];
    $build['swiper_cards'] = [
      '#theme' => 'swiper_cards',
      '#swiper_cards_data' => $swiper_cards_data,
      '#swiper_card_layout' => $swiper_card_layout,
      '#default_data' => $default_data,
    ];

    $build['#attached']['library'][] = 'swiper_cards/swiper_cards';
    return $build;
  }

}
